# TECHNICAL TEST : ANALYZE E-COMMERCE DATA

You were recently hired by an E-commerce company. Your mission is to provide insights on sales.

There are four datasets :
* *Products*: a list of available products.
* *Items*: a list of items.
* *Orders*: a list of customer orders on the website.
* *Customers*: a list of customers.

Centralize your projet and code in a Github repository and provide the url once the test is completed.

**To Dos**
1. Create and insert data in an local PostgreSQL.
2. Each day we want to compute summary statistics by customers every day (spending, orders etc.)
Create a script to compute for a given day these summary statistics.
3. Run that script over the necessary period to inject historic data. Then, identify the top customers
4. How many customers are repeaters ?
5. Package your script in Docker container so that it can be run each day. We expect a `docker-compose.yml` and a CLI to get stats for a specific day.

**Proposed solution**
- packaging in docker **(q5)**: 
to build a local database and initialize it run the following commands :
    
    ```commandline
    docker-compose build
    docker-compose up
    ```


- `db_utils.py` : class containing all util functions


- `initialise_db.py` : script that

  - creates & fills tables in local db from initial csv files after data quality check (**q 1**)
  - computes history customers stats and insert it in database **(q 3)**



- `compute_daily_customer_stats.py` compute customer stats for arg 'day', if arg --day is not given, compute stats for today
  ```commandline
  docker run -it  --network e_commerce_data_pipeline_app-tier e_commerce_data_pipeline_app python compute_daily_customer_stats.py 
  
  docker run -it  --network e_commerce_data_pipeline_app-tier e_commerce_data_pipeline_app python compute_daily_customer_stats.py --day 2018-03-24
  ```


- `get_top_customers.py` : script that identifies top 10 customers **(q3)** for these categories :
  ```commandline
  docker run -it  --network e_commerce_data_pipeline_app-tier e_commerce_data_pipeline_app python get_top_customers.py
  ```
  - by total spent amount
  - by total purchased items 


- `get_repeaters.py` : script that gets customers who bought the same product more than n times (n is a parameter) **(q4)**\
To get customers who bought products more than 10 times:

  ```commandline
  docker run -it  --network e_commerce_data_pipeline_app-tier e_commerce_data_pipeline_app python get_repeaters.py -- n
  ```

