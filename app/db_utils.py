import datetime as dt
import json

import pandas as pd
import sqlalchemy


class DBUtils:
    credentials = dict(
        host="postgres_db",
        user="postgres",
        password="docker",
        database="postgres",
        port=5432,
    )

    def __init__(self):
        self.con = self._connect_to_database(self.credentials)

    @staticmethod
    def _connect_to_database(credentials):
        """
        connect to database using credential in environment.py
        :return: connection to database
        """
        url = "postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}".format(
            **credentials
        )

        con = sqlalchemy.create_engine(url)
        return con

    def get_customers_daily_stats(self, day):
        """
        get daily stats of each customer :
            - total spent amount
            - number of purchased items
            - number of orders by category
            - list of purchased orders
        :param day: day of stats
        :return: dataframe with daily infos
        """

        columns_out = [
            "customer_unique_id",
            "day",
            "spending_amount",
            "number_of_items",
            "orders_by_category",
            "orders",
        ]
        query = """
        SELECT
            c.customer_unique_id,
            c.customer_id,
            o.order_id,
            o.order_status,
            o.order_purchase_timestamp,
            date(o.order_purchase_timestamp) AS order_purchase_day,
            i.order_item_id,
            i.product_id,
            i.price + i.freight_value AS total_purchase_price,
            p.product_category_name
        FROM
            orders o
        JOIN customer c ON
            c.customer_id = o.customer_id
        JOIN items i ON
            i.order_id = o.order_id
        JOIN products p ON
            p.product_id = i.product_id
        WHERE
            date(o.order_purchase_timestamp) = '{}';
        """.format(
            day
        )

        df = pd.read_sql(sql=query, con=self.con)

        if df.empty:
            return pd.DataFrame(columns=columns_out)

        # for each customer, build a dictionary with order category as key and number of orders by category as value
        df_orders_by_cat = (
            df.groupby(by=["customer_unique_id", "product_category_name"])["order_id"]
            .count()
            .reset_index()
            .rename(columns={"order_id": "number_of_orders"})
        )
        df_orders_by_cat["orders_by_category"] = df_orders_by_cat.apply(
            lambda x: {x.product_category_name: x.number_of_orders}, axis=1
        )
        df_orders_by_cat = (
            df_orders_by_cat.groupby("customer_unique_id")["orders_by_category"]
            .apply(list)
            .reset_index()
        )
        # for each customer, get spending_amount, number of items and list of orders
        df_stats = (
            df.groupby(by=["customer_unique_id"])
            .agg(
                {
                    "total_purchase_price": "sum",
                    "order_item_id": "count",
                    "order_id": list,
                }
            )
            .rename(
                columns={
                    "total_purchase_price": "spending_amount",
                    "order_item_id": "number_of_items",
                    "order_id": "orders",
                }
            )
            .reset_index()
        )

        df_stats = df_stats.merge(df_orders_by_cat, on=["customer_unique_id"])

        df_stats.orders_by_category = df_stats.orders_by_category.apply(json.dumps)

        return df_stats.assign(day=day)[columns_out]

    def get_repeaters(self, min_order_frequency):
        """
        Get customers who buy the same item more than n times
        :param min_order_frequency: minimum order frequency
        :return: dataframe containing information about customers who bought the same item more than n times
        """

        query = """
        WITH compute_order_frequency AS (
        SELECT
            c.customer_unique_id,
            i.product_id,
            count(o.order_id) AS order_frequency
        FROM
            orders o
        JOIN customer c ON
            c.customer_id = o.customer_id
        JOIN items i ON
            i.order_id = o.order_id
        GROUP BY
            c.customer_unique_id,
            i.product_id)
        SELECT
            *
        FROM
            compute_order_frequency
        WHERE
            order_frequency>={}
        ORDER BY
            order_frequency DESC,
            customer_unique_id
         ;
    
        """.format(
            min_order_frequency
        )
        df = pd.read_sql(query, con=self.con)
        return df

    def get_top_customers(self):
        df_top_spenders = pd.read_sql(
            """
            WITH get_total_stats AS (SELECT
                customer_unique_id,
                sum(spending_amount) AS total_amount
            FROM
                customers_daily_stats
            GROUP BY
                customer_unique_id)
            SELECT * FROM get_total_stats ORDER BY total_amount DESC LIMIT 10;
            """,
            con=self.con,
        )

        df_top_spenders.columns = pd.MultiIndex.from_product(
            [["top 10 spenders"], df_top_spenders.columns]
        )

        df_top_quantity = pd.read_sql(
            """
            WITH get_total_stats AS (SELECT
                customer_unique_id,
                sum(number_of_items) AS total_items
            FROM
                customers_daily_stats
            GROUP BY
                customer_unique_id)
            SELECT * FROM get_total_stats ORDER BY total_items DESC LIMIT 10;

            """,
            con=self.con,
        )

        df_top_quantity.columns = pd.MultiIndex.from_product(
            [["top 10 quantity buyers"], df_top_quantity.columns]
        )

        df_top_customers = pd.concat([df_top_spenders, df_top_quantity], axis=1)

        # save results to csv
        df_top_customers.to_csv(
            "outputs/top_customers_{}.csv".format(dt.datetime.now())
        )

        return df_top_customers

    @staticmethod
    def check_primary_key_constraint(df, table, primary_key):
        tmp = df.groupby(primary_key).count()
        if not tmp[tmp[tmp.columns[0]] > 1].empty:
            print(
                "WARNING: Found duplicates rows in table {} for customer(s) : {}".format(
                    table, ", ".join(tmp[tmp.customer_unique_id > 1].index.to_list())
                )
            )
        return df.drop_duplicates(subset=primary_key)

    @staticmethod
    def format_date_time_cols(df, datetime_columns):
        df_formatted = df.copy()
        for c in datetime_columns:
            df_formatted[c] = pd.to_datetime(df_formatted[c], errors="coerce")
        return df_formatted
