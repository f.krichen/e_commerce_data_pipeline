import argparse
import datetime as dt

import pandas as pd

from db_utils import DBUtils

pd.options.display.max_rows = 99
pd.options.display.max_columns = 99
pd.set_option("display.width", 1000)

# Arguments
parser = argparse.ArgumentParser(
    description="Get customers that ordered more than n times the same order"
)
parser.add_argument("--n", metavar="min order frequency", required=False, type=int)

if __name__ == "__main__":

    utils = DBUtils()

    # get day value from arguments
    args = vars(parser.parse_args())
    if args["n"] is not None:
        min_order_frequency = args["n"]
    else:
        min_order_frequency = 2

    # get customers that ordered products more than n times
    df = utils.get_repeaters(min_order_frequency)

    # show results
    print(
        "The following customers purchased orders more than {} times: \n".format(
            min_order_frequency
        )
    )
    print(df)

    # save results to csv
    df.to_csv("outputs/repeaters_{}.csv".format(dt.datetime.now()))
