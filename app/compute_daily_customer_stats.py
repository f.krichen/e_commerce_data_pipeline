import argparse
import datetime as dt

import pandas as pd

from db_utils import DBUtils

pd.options.display.max_rows = 99
pd.options.display.max_columns = 99
pd.set_option("display.width", 1000)

# Arguments
parser = argparse.ArgumentParser(description="compute daily stats")

parser.add_argument(
    "--day", metavar="YYYY-MM-DD", default=None, type=str, help="Start of report period"
)

if __name__ == "__main__":

    utils = DBUtils()

    # get day value from arguments
    args = vars(parser.parse_args())
    day = args["day"]
    if day is None:
        day = dt.date.today()

    # compute daily stats
    df = utils.get_customers_daily_stats(day)

    print("Customers daily stats")
    print(df)
    # write result to database if not already written
    # possible optimisation: implement an upsert function
    try:
        df.to_sql(
            "customers_daily_stats",
            con=utils.con,
            schema="public",
            index=False,
            if_exists="append",
        )
    except Exception as e:
        print("could not write in database, pk already exists")

    print("computed daily stats *")
