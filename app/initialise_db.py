import datetime as dt
import os

import pandas as pd
from sqlalchemy.sql import text

from db_utils import DBUtils

import time

if __name__ == "__main__":

    db_utils = DBUtils()

    # sleep 10 seconds to ensure that postgres server is completely up
    time.sleep(10)

    print("Initializing Database..")

    # region initialize database: create tables
    print("\t \t Executing DDL")
    with open("sql/create_tables.sql") as o:
        sql_ddl = o.read()

    db_utils.con.execute(text(sql_ddl))

    # read data from csv files
    path = "data/"
    customer = pd.read_csv(
        os.path.join(path, "customer.csv"), dtype={"customer_zip_code_prefix": "string"}
    )
    items = pd.read_csv(os.path.join(path, "items.csv"))
    orders = pd.read_csv(os.path.join(path, "orders.csv"))
    products = pd.read_csv(os.path.join(path, "products.csv"))
    # endregion

    # region data quality check
    print("\t \tData quality check")
    # check uniqueness
    customer = db_utils.check_primary_key_constraint(
        df=customer, table="customer", primary_key="customer_id"
    )
    orders = db_utils.check_primary_key_constraint(
        df=orders, table="orders", primary_key="order_id"
    )
    products = db_utils.check_primary_key_constraint(
        df=products, table="product", primary_key="product_id"
    )
    items = db_utils.check_primary_key_constraint(
        df=items, table="items", primary_key=["order_id", "order_item_id"]
    )

    # order column can only contain a status among the following:
    possible_order_status = [
        "shipped",
        "unavailable",
        "invoiced",
        "created",
        "approved",
        "processing",
        "delivered",
        "canceled",
    ]
    orders.loc[
        ~orders["order_status"].isin(possible_order_status), "order_status"
    ] = None

    # check that date time columns are formatted correctly and replace the bad ones with None value (same strategy can be applied to numeric columns)
    orders_datetime_columns = [
        "order_purchase_timestamp",
        "order_approved_at",
        "order_delivered_carrier_date",
        "order_delivered_customer_date",
        "order_estimated_delivery_date",
    ]
    orders = db_utils.format_date_time_cols(orders, orders_datetime_columns)
    items_date_time_columns = ["shipping_limit_date"]
    items = db_utils.format_date_time_cols(items, items_date_time_columns)

    # purchase date should not be in the future
    if not orders[orders.order_purchase_timestamp > dt.datetime.today()].empty:
        print(
            "WARNING: Found purchase dates in the future for order(s): {}".format(
                ",".join(
                    orders[
                        orders.order_purchase_timestamp > dt.datetime.today()
                        ].order_id.tolist()
                )
            )
        )

    # endregion

    # region ingest data to database
    print("\t \t Writing data in database")
    products.to_sql(
        "products", con=db_utils.con, schema="public", index=False, if_exists="append"
    )
    customer.to_sql(
        "customer", con=db_utils.con, schema="public", index=False, if_exists="append"
    )
    orders.to_sql(
        "orders", con=db_utils.con, schema="public", index=False, if_exists="append"
    )
    items.to_sql(
        "items", con=db_utils.con, schema="public", index=False, if_exists="append"
    )
    # endregion

    # region computing history customer stats
    print("\t \t Computing history stats ")
    df_dates = pd.read_sql(
        """
                            SELECT
                                date(min(order_purchase_timestamp)) AS min_day,
                                date(max(order_purchase_timestamp)) AS max_day
                            FROM
                                orders ;
                            """,
        con=db_utils.con,
    )

    min_day, max_day = df_dates.min_day[0], min(dt.date.today(), df_dates.max_day[0])

    date_range = pd.date_range(start=min_day, end=max_day, freq="D")

    for i, day in enumerate(date_range):
        print("{}/{}".format(i + 1, len(date_range)), end="\r")
        df_stats = db_utils.get_customers_daily_stats(day)
        # write results to table customers_daily_stats
        df_stats.to_sql(
            "customers_daily_stats",
            con=db_utils.con,
            schema="public",
            index=False,
            if_exists="append",
        )

    # endregion
