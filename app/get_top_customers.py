from db_utils import DBUtils
import pandas as pd

pd.options.display.max_rows = 99
pd.options.display.max_columns = 99
pd.set_option("display.width", 1000)

if __name__ == "__main__":
    print("\t \t Getting top customers: ")

    utils = DBUtils()
    # get top customers
    df_top_customers = utils.get_top_customers()
    print("Top customers : \n")
    print(df_top_customers)
