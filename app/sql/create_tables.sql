BEGIN ;
-- products
CREATE TABLE products(
product_id varchar(100),
product_category_name varchar(100),
product_name_lenght integer,
product_description_lenght NUMERIC(32,12),
product_photos_qty integer,
product_weight_g NUMERIC(32,12),
product_length_cm NUMERIC(32,12),
product_height_cm NUMERIC(32,12),
product_width_cm NUMERIC(32,12),
product_category_name_english varchar(100),
CONSTRAINT products_pk PRIMARY KEY (product_id)
);

-- customer
CREATE TABLE customer (
customer_id varchar(100),
customer_unique_id varchar(100),
customer_zip_code_prefix varchar(10),
customer_city varchar(100),
customer_state varchar(100),
CONSTRAINT customer_pk PRIMARY KEY (customer_id)
);

-- orders
CREATE TABLE orders (
order_id varchar(100) ,
customer_id varchar(100) ,
order_status varchar(100) ,
order_purchase_timestamp timestamp,
order_approved_at timestamp,
order_delivered_carrier_date timestamp,
order_delivered_customer_date timestamp,
order_estimated_delivery_date date ,
CONSTRAINT orders_pk PRIMARY KEY  (order_id),
CONSTRAINT orders_fk FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
);

-- items
CREATE TABLE items (
order_id varchar(100) ,
order_item_id integer ,
product_id varchar(100) ,
seller_id varchar(100) ,
shipping_limit_date varchar(100) ,
price NUMERIC(32,12),
freight_value NUMERIC(32,12),
CONSTRAINT items_pk PRIMARY KEY (order_id, order_item_id),
CONSTRAINT items_order_fk FOREIGN KEY (order_id) REFERENCES orders(order_id),
CONSTRAINT items_fk FOREIGN KEY (product_id) REFERENCES products(product_id)
);


-- costumer daily stats
CREATE TABLE customers_daily_stats(
customer_unique_id varchar(100),
day date,
spending_amount NUMERIC(32,12),
number_of_items integer,
orders_by_category TEXT,
orders TEXT,
CONSTRAINT cds_pk PRIMARY KEY(customer_unique_id, day)
);

COMMIT ;